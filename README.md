# README

----
## Project starten (IntelliJ IDEA)

> 1. Start IntelliJ IDEA
> 2. Druk op ``file`` en selecteer daarna ``open...``
> 3. Selecteer de ``root`` map ``(team-2)``
> 4. Eenmaal geopend, vouw de map ``fase 3`` open aan de linkerkant
> 5. Selecteer de map ``server`` en open het bestand ``package.json``
> 6. Onder ``scripts`` bevindt zich een groene knop naast ``app``, klik op de knop en selecteer vervolgens ``Run 'app'``, als dit goed gaat, is de server gestart
> 7. Open de map ``server-mail`` aan de linkerkant
> 8. Open hier ook het bestand genaamd ``package.json``
> 9. Ook hier bevindt zich een groene knop onder ``scripts`` en naast ``app``, klik op de knop en selecteer ``Run 'app'``. Hiermee start je de mail server (dit is nodig om mails te versturen/ontvangen
> 10. Zodra dit allemaal gelukt is, open de map ``src`` en selecteer ``index.html``
> 11. Als dit geopend is, zie je rechtsboven een aantal ``browsers``, selecteer uw browser en de webapplicatie wordt geopend

----
## Mailserver (PaperCut)

> Om de mail functionaliteit te gebruiken, moet u eerst de ``PaperCut client`` downloaden. Om dit te downloaden, volg de stappen hieronder...

> 1. Klik [hier](https://github.com/ChangemakerStudios/Papercut/releases) om de website te bezoeken
> 2. Klik vervolgens op ``Papercut.Setup.exe``
> 3. De client wordt nu gedownload, wacht tot het klaar is en voer de installatie uit
> 4. Zodra de client is geïnstalleerd, open het programma ``Papercut``

> Als alles goed gaat, zullen de emails die verstuurd/ontvangen worden, weergegeven worden in de ``Papercut`` programma.


----
## Account gegevens
> Hieronder bevinden zich een aantal accounts die u kunt gebruiken om in te loggen. Deze accounts bestaan uit **1** ``administrator/beheerder`` account en **2** ``reiziger`` accounts

##### Administrator account
> - **Gebruikersnaam:** ``admin``
> - **Email:** `` admin@corendon.nl ``
> - **Wachtwoord:** ``Hallo123!``

---
##### Reiziger account #1
> - **Gebruikersnaam:** ``gebruiker1``
> - **Email:** `` gebruiker1@corendon.nl ``
> - **Wachtwoord:** ``Hallo123!``

---
##### Reiziger account #2
> - **Gebruikersnaam:** ``gebruiker2``
> - **Email:** `` gebruiker2@corendon.nl ``
> - **Wachtwoord:** ``Hallo123!``
