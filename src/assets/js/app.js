//Global variables
var session = sessionManager();
var databaseManager = databaseManager();
var mailConfig = mailConfig();

//Constants (sort of)
var CONTROLLER_HEADER = "header";
var CONTROLLER_USERHEADER = "userheader";
var CONTROLLER_ADMINHEADER = "adminheader";
var CONTROLLER_DASHBOARD = "dashboard";
var CONTROLLER_LOGIN = "login";
var CONTROLLER_LOGOUT = "logout";
var CONTROLLER_PROFILE = "profile";
var CONTROLLER_CONTACT = "contact";
var CONTROLLER_REGISTER = "register";
var CONTROLLER_MATCHES_LIJST = "matches-lijst";
var CONTROLLER_MATCHES = "matches";
var CONTROLLER_GESPREKKEN = "gesprekken";
var CONTROLLER_CHAT = "chat";
var CONTROLLER_QUERY = "query";
var CONTROLLER_BEHEERDER = "beheerder";
var CONTROLLER_HANDLEIDING = "handleiding";
var CONTROLLER_GUEST_HANDLEIDING = "guestHandleiding";

//This is called when the browser is done loading
$(function() {
    if(isUserLoggedIn() && getUserRol() == 2){
        loadController(CONTROLLER_ADMINHEADER);
    }else if(isUserLoggedIn() && getUserRol() == 1){
        loadController(CONTROLLER_USERHEADER);
    }else{
        loadController(CONTROLLER_HEADER);
    }

    //Attempt to load the controller from the URL, if it fails, fall back to the dashboard controller.
    loadControllerFromUrl(CONTROLLER_DASHBOARD);

    //Setup the database manager
    databaseManager.connect("http://localhost:8080/");
    databaseManager.authenticate("yourtokenhere");

    mailConfig.connect("http://localhost:8081/");
});

//This function is responsible for creating the controllers of all views
function loadController(name, controllerData) {
    // console.log("loadController: " + name);

    if(controllerData) {
        console.log(controllerData);
    }
    else {
        controllerData = {};
    }

    switch(name) {
        case CONTROLLER_DASHBOARD:
            setCurrentController(name);
            dashboardController();
            break;

        case CONTROLLER_BEHEERDER:
            setCurrentController(name);
            beheerderController();
            break;

        case CONTROLLER_QUERY:
            setCurrentController(name);
            queryController();
            break;

        case CONTROLLER_HEADER:
            headerController();
            break;

        case CONTROLLER_USERHEADER:
            userheaderController();
            break;

        case CONTROLLER_ADMINHEADER:
            adminheaderController();
            break;

        case CONTROLLER_HANDLEIDING:
            setCurrentController(name);
            handleidingController();
            break;

        case CONTROLLER_GUEST_HANDLEIDING:
            setCurrentController(name);
            guestHandleidingController();
            break;

        case CONTROLLER_CONTACT:
            setCurrentController(name);
            contactController();
            break;

        case CONTROLLER_REGISTER:
            setCurrentController(name);
            registerController();
            break;

        case CONTROLLER_LOGIN:
            setCurrentController(name);
            isLoggedIn(dashboardController, loginController);
            break;

        case CONTROLLER_LOGOUT:
            setCurrentController(name);
            handleLogout();
            loadController(CONTROLLER_HEADER);
            break;

        case CONTROLLER_MATCHES_LIJST:
            setCurrentController(name);
            isLoggedIn(matchesLijstController, loginController);
            break;

        case CONTROLLER_MATCHES:
            setCurrentController(name);
            isLoggedIn(matchesController, loginController);
            break;

        case CONTROLLER_GESPREKKEN:
            setCurrentController(name);
            isLoggedIn(gesprekkenController, loginController);
            break;

        case CONTROLLER_CHAT:
            setCurrentController(name);
            isLoggedIn(chatController, loginController);
            break;

        case CONTROLLER_PROFILE:
            setCurrentController(name);
            isLoggedIn(profileController, loginController);
            break;

        default:
            return false;
    }

    return true;
}

function loadControllerFromUrl(fallbackController) {
    var currentController = getCurrentController();

    if(currentController) {
        if(!loadController(currentController)) {
            loadController(fallbackController);
        }
    }
    else {
        loadController(fallbackController);
    }
}

function getCurrentController() {
    return location.hash.slice(1);
}

function setCurrentController(name) {
    location.hash = name;
}

//Convenience functions to handle logged-in states
function isLoggedIn(whenYes, whenNo) {
    if(session.get("username")) {
        whenYes();
    }
    else {
        whenNo();
    }
}

// returns current logged in username
function getCurrentLoggedInUser() {
    var user = session.get("username");

    if(user){
        return user;
    }
}

// returns current logged in userID
function getCurrentLoggedInUserID() {
    var userID = session.get("userID");

    if(userID){
        return userID;
    }
}
// returns current logged in users role
function getUserRol() {
    var rolID = session.get("rolID");

    if(rolID){
        return rolID;
    }
}

// returns last user chat
function getUserChat() {
    var userChatId = session.get("chatUserId");

    if(userChatId){
        return userChatId;
    }
}

// returns true if user is logged in
function isUserLoggedIn() {
    var user = session.get("username");
    var userID = session.get("userID");
    var rolID = session.get("rolID");

    if(user && userID && rolID){
        return true;
    }
}

function handleLogout() {
    session.remove("username");
    session.remove("userID");
    session.remove("rolID");

    loginController();
}
