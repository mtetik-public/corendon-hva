function registerController() {

    function initialize() {
        $.get("views/register.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        registerView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="register"]').addClass('active');

        registerView.find('.login-link').on('click', function() {
            loadController("login");

            return false;
        });

        registerView.find('.register-button').on('click', function() {
            handleRegister();

            return false;
        });

        $(".content").empty().append(registerView);

        registerView.on('load', passwordRequirement());

    }

    // Password requirement check
    function passwordRequirement() {
        var password = document.getElementById("password");
        var confirm_password = document.getElementById("confirm_password");

        function validatePassword() {
            if (password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Wachtwoorden zijn niet hetzelfde");
            } else {
                confirm_password.setCustomValidity('');
            }
        }
        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
        var letter = document.getElementById("letter");
        var capital = document.getElementById("capital");
        var number = document.getElementById("number");
        var length = document.getElementById("length");

        // When the user clicks on the password field, show the message box
        password.onfocus = function() {
            document.getElementById("message").style.display = "block";
        }

        // When the user clicks outside of the password field, hide the message box
        password.onblur = function() {
            document.getElementById("message").style.display = "none";
        }

        // When the user starts to type something inside the password field
        password.onkeyup = function() {
            // Validate lowercase letters
            var lowerCaseLetters = /[a-z]/g;
            if (password.value.match(lowerCaseLetters)) {
                letter.classList.remove("invalid");
                letter.classList.add("valid");
            } else {
                letter.classList.remove("valid");
                letter.classList.add("invalid");
            }

            // Validate capital letters
            var upperCaseLetters = /[A-Z]/g;
            if (password.value.match(upperCaseLetters)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
            }

            // Validate numbers
            var numbers = /[0-9]/g;
            if (password.value.match(numbers)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
            }

            // Validate length
            if (password.value.length >= 8) {
                length.classList.remove("invalid");
                length.classList.add("valid");
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
            }
        }
    }

    // check if the fields are filled in
    function handleRegister() {
        if($('#username').val() &&
            $('#password').val() &&
            $('#confirm_password').val() &&
            $('#email').val()){
            // Regex for password check
            var validatePassword = /^(?=.*[A-Z].*[a-z])(?=.*[0-9]).{8,}$/;
            if(validatePassword.test($('#password').val())){
                // Regex for valid email
                var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
                if (testEmail.test($('#email').val())){
                    var name = $('#firstname').val();
                    var lastname = $('#lastname').val();
                    var tussenvoegsel = $('#tussenvoegsel').val();
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var email = $('#email').val();
                    var birthday_string = $('#birthday').val();
                    var foto = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABfCAAAAAA/we3bAAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAAAEgAAABIAEbJaz4AAAPESURBVGje7dlbU9pAFADg/v8/kLMkXKIo2tpSq1YQ6mi12g7KoLapxRu0VQoqgkK5hCQNSlVgs9mz8JidYXhJ8nE2ezuHFxZ361SOt5dn1YBPCYajq5mLhsl/7wvO68yqFp9WCADpNgDwhV5v/W6NlzFKm9NEeiAeG4B/Xvs7Rqa8MQEDRk/yvTlsj4lpHUToyD0kx4pjYSpJnyNyD4W1zujM+RwT6TrKZmNU5mzKTem2ZH005nSCRyEQr4/C/A5zKbaz2hRnrl9yKnb7bIgyzRi/QvxHokyaH7G7baYsxpQmEcHYzpohwnSSKIWQQF6E+RXAKURa1vEMOhh7FOTxTCmEZqQPBprZQSv2InqDZZpRPEPINyxTDAooUtJEMt8EFAKzdSSzJok4gQKO0d+JvBpCsjimPiPEwA6OKfNtZ0Nj4COOuQwJ9Zn0wUQxBeyC1mPiBo7xizExHFMSmZ2M+ek0BFSxIbCOGwK1iNiATuGY9rzY9DzEMeaq0GLjP8cx1r5INBC5QzJCE8dx2jgyDddEgNb2LCRjfRbYpCeu0MwFfoJK8Q6a0ZfR4SgnFpqxzhRsny20BJg2NhyFkRMwjurnuAMhrOhCjLmNYWCSlbiz8ps65twhf7UEGesPVxr9EMy6LsxYJ7yvB5bZqTSbMbUAX8I+X7VGYCzjO088sFixRmIs88T1/QBJ3FkjMnZusEDYpaHgTtP1IRyFrvoXxvkDSDRnuD+Dp2xnXsT8dAhgetetKsTN2Mt1Ph4aKt2B5JtNVfjqqnTGrOWy/cVFvZiKhogkga3ZH5BAnkpk7/oR82euZnIzRmFzRpYzgze0StrG0qupiZAajkQTqdzt0DvJqfLMRsHgY8rr3Q6C4A/K5Z3G7c3VdaXWov3oQqQbaWidUrwZYoyj3oET1GOubn9sxV5ZDCJHQwENMp304/ICahZRN7cuHotvEEgPHgoGGGNXfhpQENx3r8n2mnk6/exGeddgMt/7Jggonzir5u1M39oHfo3FFIdqm4t/eDqunJT7b4Rw0Zlpx4cmO0ymXQNqacN5N8Tbjswp5cwE5O0x828APf9epixFyqkTo8eo2QYoS8eOZfNWLk7f+fqLeM+ZolOWDvKbdImy1+tXe/OK0+odKjowGcZ6D+pCKl95mvxmu/pzZ1FlbEWQoTPGCjNDA1DC0cRW+kDTDtJbiWhYAWDtd9KKQWXcUxp7YZa6j/7/zb54rkFlqrx/CPA1CFepzLU6TsUeA9dU5lKssuHYgiWP8RiP8RiP8RiP8RiPEWb+AQYEvomvdJALAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTAxLTA4VDA1OjE5OjE2LTA1OjAwHqpjigAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wMS0wOFQwNToxOToxNi0wNTowMG/32zYAAAAASUVORK5CYII=";

                    if (birthday_string == ""){
                        var birthday = "0000-00-00";
                    } else {
                        var birthday = birthday_string;
                    }

                    var residence = $('#residence').val();

                    // if password is the same as confirm password then hash the password
                    if(password == $('#confirm_password').val()) {
                        // hash the password
                        var hashPassword = sha256_digest(password);

                        // kijk of gebruikersnaam of email in de database al bestaat, zo niet voer dan de gegevens in de database
                        databaseManager
                            .query("SELECT * FROM gebruiker WHERE gebruikersnaam = '" + username + "' OR email = '" + email + "'")
                            .done(function (data) {
                                if (data.length < 1) {
                                    databaseManager
                                    .query("INSERT INTO gebruiker (gebruikersnaam, voornaam, tussenvoegsel, achternaam, geboortedatum, woonplaats, wachtwoord, email, registratie_datum, foto) VALUES(?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?)",
                                    [username, name, tussenvoegsel, lastname, birthday, residence, hashPassword, email, foto])
                                        .done(function (data) {
                                            $('.register-button').html('<i class="fa fa-spinner fa-spin"></i> Gegevens controlleren');
                                            $('.register-button').prop('disabled', true);
                                            $('.register-button').css('cursor', 'not-allowed');

                                            setTimeout(function () {
                                                $('.register-button').html('<i class="fa fa-spinner fa-spin"></i> Inloggen');
                                                $('.register-button').css('background-color', 'lightgray');

                                                databaseManager
                                                    .query("SELECT * FROM gebruiker WHERE gebruikersnaam = '" + username + "'")
                                                    .done(function (data) {
                                                        setTimeout(function () {
                                                            // set id's and give rol
                                                            session.set("username", username);
                                                            session.set("userID", data[0].idGebruiker);
                                                            session.set("rolID", data[0].idRol);

                                                            loadController(CONTROLLER_USERHEADER);
                                                            loadController(CONTROLLER_DASHBOARD);
                                                        }, 2500);
                                                    })
                                                    .fail(function (reason) {
                                                        console.log(reason);
                                                    });
                                            }, 1500);
                                        }).fail(function (reason) {
                                        console.log(reason);
                                    });
                                } else {
                                    alert("De door u ingevoerde gegevens bestaan al.");
                                }
                            })
                            .fail(function (reason) {
                                console.log(reason);
                            });
                    } else {
                        alert("Wachtwoorden komen niet overeen!");
                    }
                } else {
                    alert("voer een geldige email in!");
                    $('.required-field').css('border', '2px solid red');
                }
            } else {
                alert("Wachtwoord voldoet niet aan de eisen!");
                $('.required-field').css('border', '2px solid red');
            }
        } else {
            alert("Vul de verplichte velden in!")
            $('.required-field').css('border', '2px solid red');
        }
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
