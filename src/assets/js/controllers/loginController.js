function loginController() {
    //Reference to our loaded view
    var loginView;

    function initialize() {
        $.get("views/login.html")
            .done(setup)
            .fail(error);
    }

    //Called when the login.html has been loaded
    function setup(data) {
        //Load the login-content into memory
        loginView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="login"]').addClass('active');

        loginView.find(".login-form .login-submit").on("click", function() {
            handleLogin();

            //Return false to prevent the form submission from reloading the page.
            return false;
        });

        loginView.find(".reset-link").on("click", function() {
            handlePopup();
        });

        loginView.find(".reset-submit_step1").on("click", function(e) {
            e.preventDefault();

            handleUserCheck();
        });

        loginView.find(".reset-submit_step2").on("click", function(e) {
            e.preventDefault();

            handleCodeCheck();
        });

        loginView.find(".reset-submit_step3").on("click", function(e) {
            e.preventDefault();

            handlePasswordChange();
        });

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(loginView);
    }

    function handleUserCheck() {
        var errorMessage = "";
        var input = $(".reset-username").val();

        if(input){
            databaseManager.query(
                "SELECT * FROM gebruiker WHERE email = ?",
                [input]
            )
            .done(function(data) {
                if(data.length > 0){
                    var naam = data[0].gebruikersnaam;
                    var generatedCode = "";

                    // generate 4 digit code
                    for (var i = 1; i <= 4; i++) {
                        generatedCode += Math.floor((Math.random() * 10));
                    }

                    // if mail doesnt work, check console log
                    console.log(generatedCode);

                    // change password to the 4 digit code
                    databaseManager.query("UPDATE gebruiker SET wachtwoord = ? WHERE email = ?", [generatedCode, data[0].email])
                    .done(function(data) {
                        $('.reset-submit_step1').html('<i class="fa fa-spinner fa-spin"></i> Verzenden');

                        setTimeout(function() {
                            $(".errorMessage").html(
                                "<h3>Er is een mail verstuurd naar uw emailadres dat bij ons bekend is." +
                                "<br>Voer de code in die u per mail heeft gekregen.</h3>"
                            );
                            $(".codeInput").css("display", "block");
                            $(".reset-submit_step1").html("Verzenden").css("display", "none");

                        }, 1000);
                    }).fail(function(reason) {
                        console.log(reason);
                    });

                    var email = data[0].email;
                    var onderwerp = "Wachtwoord vergeten";
                    var bericht = "Beste " + naam + ",<br><br/>" +
                                "U heeft onlangs een aanvraag ingediend om uw wachtwoord te wijzigen.<br>" +
                                "Hierbij ontvangt u de code die noodzakelijk is om uw wachtwoord te wijzigen.<br><br/>" +
                                "Uw code: <strong>" + generatedCode + "</strong>";

                    $(".__email").prop('disabled', true).css("cursor", "not-allowed");

                    // send mail
                    mailConfig.sendMail("Corendon <info@corendon.nl>", email, onderwerp, bericht);
                }else{
                    errorMessage = "<h3>Er bestaat geen gebruiker met de door u ingevoerde gegevens.</h3>";
                    $(".errorMessage").html(errorMessage);
                }
            }).fail(function(reason) {
                console.log(reason);
            });
        }else{
            errorMessage = "<h3>Vul uw email in.</h3>";
            $(".errorMessage").html(errorMessage);
        }
    }

    function handleCodeCheck() {
        var email = $(".__email").val();
        var code = $(".__code").val();

        if(code) {
            // checks if the 4 digit code matches with the email
            databaseManager.query("SELECT * FROM gebruiker WHERE email = ? AND wachtwoord = ? LIMIT 1", [email, code])
            .done(function (data){
                if(data.length > 0) {
                    $(".__code").prop("disabled", true).css("cursor", "not-allowed");
                    $(".errorMessage").html("");

                    $('.reset-submit_step2').html('<i class="fa fa-spinner fa-spin"></i> Code controleren');

                    setTimeout(function() {
                        $(".errorMessage").html(
                            "<h3>Kies een nieuwe wachtwoord.</h3>"
                        );
                        $(".passwordInput").css("display", "block");
                        $(".reset-submit_step2").html("Code controleren").css("display", "none");
                    }, 1000);
                }else {
                    errorMessage = "<h3>Code is verkeerd, probeer het opnieuw.</h3>";
                    $(".errorMessage").html(errorMessage);
                }
            }).fail(function (reason){
                console.log(reason);
            });
        }else{
            errorMessage = "<h3>Vul uw code in.</h3>";
            $(".errorMessage").html(errorMessage);
        }
    }

    function handlePasswordChange() {
        // get the value of these fields
        var email = $(".__email").val();
        var code = $(".__code").val();
        var wachtwoord = $(".__wachtwoord").val();
        var confirm_wachtwoord = $(".__confirm_wachtwoord").val();

        // if password is same as confirme password
        if(wachtwoord == confirm_wachtwoord) {
            if(wachtwoord.length > 6){
                // checks if the 4 digit code matches with the email
                databaseManager.query("SELECT * FROM gebruiker WHERE email = ? AND wachtwoord = ?", [email, code])
                .done(function (data){
                    if(data.length > 0) {
                        var naam = data[0].gebruikersnaam;

                        // hash the password
                        var hashedWachtwoord = sha256_digest(wachtwoord);
                        // change the code to the hashed password
                        databaseManager.query("UPDATE gebruiker SET wachtwoord = ? WHERE email = ? AND wachtwoord = ?", [hashedWachtwoord, email, code])
                        .done(function (data){
                            var onderwerp = "Wachtwoord gewijzigd";
                            var bericht = "Beste " + naam + ",<br><br/>Uw wachtwoord is succesvol gewijzigd.";

                            $('.reset-submit_step3').html('<i class="fa fa-spinner fa-spin"></i> Wachtwoord wijzigen');

                            // send mail
                            mailConfig.sendMail("Corendon <info@corendon.nl>", email, onderwerp, bericht);

                            // reload page
                            setTimeout(function(){
                                location.reload();
                            }, 1500)
                        }).fail(function (reason){
                            console.log(reason);
                        });
                    }else{
                        errorMessage = "<h3>Onbekende fout. [error: 901]</h3>";
                        $(".errorMessage").html(errorMessage);
                    }
                }).fail(function (reason){
                    console.log(reason);
                });
            }else{
                errorMessage = "<h3>Uw wachtwoord is te kort (minimaal 6 tekens).</h3>";
                $(".errorMessage").html(errorMessage);
            }
        }else{
            errorMessage = "<h3>Wachtwoorden komen niet overeen, probeer het opnieuw.</h3>";
            $(".errorMessage").html(errorMessage);
        }
    }

    function handlePopup() {
        $("#popupPasswordReset").css("display", "block");

        $("span.close").on("click", function(){
            $("#popupPasswordReset").css("display", "none");
        });
    }

    function handleLogin() {
        // check the value from inputs
        var loginMethod = $('.login-name').val();
        var password = $('.login-password').val();

        // hash password to check if the hash is the same in database
        var hashPasswordLogin = sha256_digest(password);

        // if filled in, then check if username exist in the database then direct to home page
        if(loginMethod && password) {
            databaseManager.query(
                "SELECT * FROM gebruiker WHERE (gebruikersnaam = ? AND wachtwoord = ?) OR (email = ? AND wachtwoord = ?) LIMIT 1",
                [loginMethod, hashPasswordLogin, loginMethod, hashPasswordLogin]
            ).done(function (data) {
                if(data.length > 0) {
                    if(data[0].gebruiker_actief == '1') {
                        $(".login-submit").html('<i class="fa fa-spinner fa-spin"></i> Inloggen');

                        // wait 2 seconds and then direct to home page
                        setTimeout(function() {
                            if (data[0].idRol == 2) {
                                loadController(CONTROLLER_ADMINHEADER);
                            } else {
                                loadController(CONTROLLER_USERHEADER);
                            }
                            loadController(CONTROLLER_DASHBOARD);
                        }, 2000);

                        //Set the username in the global session variable
                        session.set("username", data[0].gebruikersnaam);
                        //Set the userID inthe global session variable
                        session.set("userID", data[0].idGebruiker);
                        //Set the rolID inthe global session variable
                        session.set("rolID", data[0].idRol);
                    } else {
                        alert("Uw toegang tot de applicatie is u ontnomen.");
                    }
                    // if username or password is not correct, alert
                } else {
                    alert("Gebruikersnaam en/of wachtwoord incorrect!");
                    $('.required-field').css('border', '2px solid red');
                }
            }).fail(function (reason) {
                console.log(reason);
            });
        } else {
            alert("Voer een gebruikersnaam en/of wachtwoord in!");
        }
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

    //Run the initialize function to kick things off
    initialize();
}
