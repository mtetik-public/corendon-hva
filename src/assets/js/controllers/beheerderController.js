function beheerderController() {
    //Reference to our loaded view
    var beheerderView;

    function initialize() {
        $.get("views/beheerder.html")
            .done(setup)
            .fail(error);
    }

    //Called when the beheerder.html has been loaded
    function setup(data) {
        //Load the beheerder-content into memory
        beheerderView = $(data);

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(beheerderView);

        // Add active class to navigation
        $('#navigation-list li a').removeClass('active');
        $('[data-controller="beheerder"]').addClass('active');

        // Create a new date to get the data from this year
        var date = new Date();
        var year = date.getFullYear();

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var registations = [];
        var verzoeken = [];
        var contacten = [];

        // Loop trough all 12 months of the year and get the data off every month and add it to the concerning arrays
        $.each(months, function(index, value){
          var i = index;
          index = index + 1;
          // Run the query to get the concerning data
          databaseManager.query("SELECT COUNT(idGebruiker) FROM gebruiker WHERE MONTH(registratie_datum) = ? AND YEAR(registratie_datum) = ?", [index, year])
              .done(function (data) {
                var data = data[0];
                // add the data to the concerning array
                  $.each(data, function(key, value){
                    registations[i] = value;
                })
                var trace1 = {
                    type: 'bar',
                    x: months,
                    y: registations,
                    marker: {
                        color: '#D81E05',
                        line: {
                            width: 1
                        }
                    }
                };

                var data = [trace1];

                var layout = {
                  title: "aantal registraties " + year,
                  height: 260,
                    font: {
                        size: 18
                    }
                };

                Plotly.newPlot('registraties', data, layout, {
                    responsive: true,
                    displayModeBar: false
                });
              })
              .fail(function (reason) {
                 console.log(reason);
              });

        // Run the query to get the concerning data
        databaseManager.query("SELECT COUNT(idVerzoek) FROM verzoek WHERE MONTH(datum) = ? AND YEAR(datum) = ?", [index, year])
            .done(function (data) {
              var data = data[0];
              // add the data to the concerning array
                $.each(data, function(key, value){
                  verzoeken[i] = value;
                })
              var trace1 = {
                  type: 'bar',
                  x: months,
                  y: verzoeken,
                  marker: {
                      color: '#D81E05',
                      line: {
                          width: 1
                      }
                  }
              };

              var data = [trace1];

              var layout = {
                title: "Aantal verzoeken gestuurd " + year,
                height: 260,
                  font: {
                      size: 18
                  }
              };

              Plotly.newPlot('verzoeken', data, layout, {
                  responsive: true,
                  displayModeBar: false
              });
            })
            .fail(function (reason) {
                console.log(reason);
            });

        // Run the query to get the concerning data
      databaseManager.query("SELECT COUNT(idVerzoek) FROM verzoek WHERE MONTH(datum) = ? AND YEAR(datum) = ? AND idStatus = ?", [index, year, 2])
          .done(function (data) {
            var data = data[0];
            // add the data to the concerning array
              $.each(data, function(key, value){
                contacten[i] = value;
            })
            var trace1 = {
                type: 'bar',
                x: months,
                y: contacten,
                marker: {
                    color: '#D81E05',
                    line: {
                        width: 1
                    }
                }
            };

            var data = [trace1];

            var layout = {
              title: "aantal geaccepteerde verzoeken " + year,
              height: 260,
                font: {
                    size: 18
                }
            };

            Plotly.newPlot('contacten', data, layout, {
                responsive: true,
                displayModeBar: false
            });
          })
          .fail(function (reason) {
              console.log(reason);
          });
      });
      // Gives the button active class, shows the concerning graph and hides the other graphs
      $("a#totale-registraties").on("click", function(){
          $("a.statistics-control").removeClass("active");
          $(this).addClass("active");
          $('#verzoeken').hide();
          $('#contacten').hide();
          $('#registraties').show();
      });
      // Gives the button active class, shows the concerning graph and hides the other graphs
      $("a#totale-verzoeken").on("click", function(){
          $("a.statistics-control").removeClass("active");
          $(this).addClass("active");
          $('#registraties').hide();
          $('#contacten').hide();
          $('#verzoeken').show();
      });
      // Gives the button active class, shows the concerning graph and hides the other graphs
      $("a#totale-contacten").on("click", function(){
          $("a.statistics-control").removeClass("active");
          $(this).addClass("active");
          $('#verzoeken').hide();
          $('#registraties').hide();
          $('#contacten').show();
      });
      // Get all users data from the database
        databaseManager.query("SELECT gebruikersnaam, idGebruiker, gebruiker_actief FROM gebruiker WHERE idRol <> ?", [2])
            .done(function (data) {
                // Loop trough all user data and show the usernames
                $.each(data, function(key,value){
                  $('div[class*="user"].inactief').hide();
                  if (value.gebruiker_actief == 1) {
                    $('#users').append('<div class="user-' + value.idGebruiker + ' actief">' + value.gebruikersnaam + '<a href="javascript:void(0)" id="delete-' + value.idGebruiker + '" class="delete btn btn-red">Delete</a></div>');
                  } else if (value.gebruiker_actief == 0) {
                    $('#users').append('<div class="user-' + value.idGebruiker + ' inactief">' + value.gebruikersnaam + '<a href="javascript:void(0)" id="revert-' + value.idGebruiker + '" class="revert btn btn-green">Revert</a></div>');
                  }
                })
                // Get the id of the user to delete
                $('#users div[class*="user"] a').on("click", function(){
                      var delete_id = $(this).attr('id').replace(/[^0-9]/gi, '');
                      if ($(this).hasClass("delete")) {
                        var gebruiker_actief = 0;
                      } else if ($(this).hasClass("revert")) {
                        var gebruiker_actief = 1;
                      }
                      // Change the user status to active or inactive
                      databaseManager.query("UPDATE gebruiker SET gebruiker_actief = ? WHERE idGebruiker = ?", [gebruiker_actief, delete_id])
                          .done(function (data) {
                              location.reload();
                          })
                          .fail(function (reason) {
                              console.log(reason);
                          })
               });
               // Change the between the active and inactive users
               $("a#active-users").on("click", function(){
                   $("a.gebruiker-control").removeClass("active");
                   $(this).addClass("active");
                   $('div[class*="user"].inactief').hide();
                   $('div[class*="user"].actief').show();
               });
               // Change the between the active and inactive users
               $("a#inactive-users").on("click", function(){
                   $("a.gebruiker-control").removeClass("active");
                   $(this).addClass("active");
                   $('div[class*="user"].actief').hide();
                   $('div[class*="user"].inactief').show();
            });
          })
            .fail(function (reason) {
                $("#users").html(reason);
            });
        // Get all contact messages from the database
        databaseManager.query("SELECT * FROM contact")
            .done(function (data) {
                // Loop trough and show all the contact messages if not dealt with already
                $.each(data, function(key,value){
                  if (value.opmerking != "") {
                    if (value.status == 0) {
                      $("#user-comments").append('<div class="comment ' + value.onderwerp +' comment-' + value.idContact + ' actief"><b>' + value.onderwerp + '</b><p>' + value.opmerking + '</p><a href="javascript:void(0)" id="response-' + value.idContact + '" class="response btn btn-green">Reageren</a></div>');
                    } else if (value.status == 1) {
                      $("#user-comments").append('<div class="comment ' + value.onderwerp +' comment-' + value.idContact + ' inactief"><b>' + value.onderwerp + '</b><p>' + value.opmerking + '</p></div>');
                    }
                  }
                  $('div[class*="comment"].inactief').hide();
                });
                // Pop up a response window and get all the input values
                  $("a.response").on("click", function(){
                    var response_id = $(this).attr('id').replace(/[^0-9]/gi, '');
                    $("#responsePopup button").attr("id", "uniek-" + response_id);
                    databaseManager.query("SELECT * FROM contact WHERE idContact = ?", [response_id])
                      .done(function (data) {
                        $("input.input-response.voornaam").val(data[0].voornaam);
                        $("input.input-response.tussenvoegsel").val(data[0].tussenvoegsel);
                        $("input.input-response.achternaam").val(data[0].achternaam);
                        $("input.input-response.email").val(data[0].email);
                        $("span.contact-type").text(data[0].onderwerp);

                        $("#responsePopup").css('display', 'block');
                      }).fail(function (reason) {
                          console.log("Beheerder: Fout");
                      });
                    });
                        // Send all the response values to the concerning email
                        $("#responsePopup button").on("click", function(){
                          if ($("textarea.input-response").val() != "") {
                            var achternaam = $("input.input-response.achternaam").val();
                            var antwoord = $("textarea.input-response").val();
                            var email = $("input.input-response.email").val();
                            var onderwerp = $("span.contact-type").text();

                            $("#responsePopup button").html('<i class="fa fa-spinner fa-spin"></i> Reageren');

                            setTimeout(function () {
                                mailConfig.sendMail("Correndon <info@correndon.nl>", email, "Uw " + onderwerp + " op Corendon.", "Beste heer/mevrouw " + achternaam + ", <br>" + antwoord);

                                $("#responsePopup button").html('Reageren');
                                $("#responsePopup").fadeOut();
                                $("textarea.input-response").val("");
                            }, 1000);
                            var response_id = $(this).attr('id').replace(/[^0-9]/gi, '');
                            // Change the comment status to "Dealt with"
                            databaseManager.query("UPDATE contact SET status = ? WHERE idContact = ?", [1, response_id])
                              .done(function (data) {
                                console.log(data);
                                  location.reload();
                              }).fail(function (reason) {
                                  console.log("Beheerder: Fout");
                              });
                            } else {
                              $("textarea.input-response").css("border", "1px solid red");
                            }
                        });
                  $(".close").on("click", function(){
                      $("#responsePopup").css('display', 'none');
                  });
                  	// Change the comments categorie
                  $("a#vragen").on("click", function(){
                      $("a.comment-control").removeClass("active");
                      $(this).addClass("active");
                      $('div.comment:not(.vraag), div.comment.inactief').hide();
                      $('div.comment.vraag:not(.inactief)').show();
                      // Check if there are any comments to show. If not tell the user there are none to show
                      if ($('div.comment.vraag').filter(':visible').length == 0) {
                          $('p.niet-aanwezig').text("Er zijn nog geen vragen.");
                      } else {
                          $('p.niet-aanwezig').text("");
                      }
                  });
                  // Change the comments categorie
                  $("a#opmerkingen").on("click", function(){
                      $("a.comment-control").removeClass("active");
                      $(this).addClass("active");
                      $('div.comment:not(.opmerking), div.comment.inactief').hide();
                      $('div.comment.opmerking:not(.inactief)').show();
                      // Check if there are any comments to show. If not tell the user there are none to show
                      if ($('div.comment.opmerking').filter(':visible').length == 0) {
                          $('p.niet-aanwezig').text("Er zijn nog geen opmerkingen.");
                      } else {
                          $('p.niet-aanwezig').text("");
                      }
                  });
                  // Check if there are any comments to show. If not tell the user there are none to show
                  if ($('div.comment').filter(':visible').length == 0) {
                      $('p.niet-aanwezig').text("Er zijn nog geen reacties.");
                  } else {
                      $('p.niet-aanwezig').text("");
                  }
                  // Change the comments categorie
                  $("a#alles").on("click", function(){
                      $("a.comment-control").removeClass("active");
                      $(this).addClass("active");
                      $('div.comment.inactief').hide();
                      $('div.comment:not(.inactief)').show();
                      // Check if there are any comments to show. If not tell the user there are none to show
                      if ($('div.comment').filter(':visible').length == 0) {
                          $('p.niet-aanwezig').text("Er zijn nog geen reacties.");
                      } else {
                          $('p.niet-aanwezig').text("");
                      }
                  });
                  // Change the comments categorie
                  $("a#klachten").on("click", function(){
                      $("a.comment-control").removeClass("active");
                      $(this).addClass("active");
                      $('div.comment:not(.klacht), div.comment.inactief').hide();
                      $('div.comment.klacht:not(.inactief)').show();
                      // Check if there are any comments to show. If not tell the user there are none to show
                      if ($('div.comment.klach').filter(':visible').length == 0) {
                          $('p.niet-aanwezig').text("Er zijn nog geen klachten.");
                      } else {
                          $('p.niet-aanwezig').text("");
                      }
                  });
                  // Change the comments categorie
                  $("a#afgehandeld").on("click", function(){
                      $("a.comment-control").removeClass("active");
                      $(this).addClass("active");
                      $('div.comment:not(.inactief)').hide();
                      $('div.comment.inactief').show();
                      // Check if there are any comments to show. If not tell the user there are none to show
                      if ($('div.comment.inactief').filter(':visible').length == 0) {
                          $('p.niet-aanwezig').text("Er is nog niets afgehandeld.");
                      } else {
                          $('p.niet-aanwezig').text("");
                      }
                  });
            })
            .fail(function (reason) {
                console.log("Beheerder: Fout");
            });
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

    //Run the initialize function to kick things off
    initialize();
}
