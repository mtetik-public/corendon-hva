function handleidingController() {
    var handleiding;

    function initialize() {
        $.get("views/handleiding.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        handleiding = $(data);

        // Remove previous nav item class 'active'
        // Set current nav item on active
        $('#navigation-list li a').removeClass('active');
        $('[data-controller="handleiding"]').addClass('active');

        // Retrieve data of current logged in user
        databaseManager.query(
            "SELECT * FROM gebruiker INNER JOIN rol ON gebruiker.idRol = rol.idRol WHERE gebruiker.gebruikersnaam = ? OR gebruiker.email = ? LIMIT 1",
            [getCurrentLoggedInUser(), getCurrentLoggedInUser()]
        )
        .done(function(data) {
            // Get role id of current user
            switch (data[0].idRol) {
                // if role id = 1
                case 1:
                    // Get user guide (NL)
                    changeGuide("userGuideNL.html");

                    // On click, set new language on active
                    // And change user guide language to NL
                    handleiding.find("#nlTranslation").on("click", function(){
                        translateGuide("#nlTranslation", "#enTranslation");

                        changeGuide("userGuideNL.html");
                    });

                    // On click, set new language on active
                    // And change user guide language to EN
                    handleiding.find("#enTranslation").on("click", function(){
                        translateGuide("#enTranslation", "#nlTranslation");

                        changeGuide("userGuideEN.html");
                    });

                    break;
                // if role id = 2
                case 2:
                    // Get admin guide (NL)
                    changeGuide("adminGuideNL.html");

                    // On click, set new language on active
                    // And change admin guide language to NL
                    handleiding.find("#nlTranslation").on("click", function(){
                        translateGuide("#nlTranslation", "#enTranslation");

                        changeGuide("adminGuideNL.html");
                    });

                    // On click, set new language on active
                    // And change admin guide language to EN
                    handleiding.find("#enTranslation").on("click", function(){
                        translateGuide("#enTranslation", "#nlTranslation");

                        changeGuide("adminGuideEN.html");
                    });

                    break;
                // if role id is not 1 or 2
                default:
                    $(".handleiding-text").html("NOTHING TO DISPLAY");
            }
        }).fail(function(reason) {
            console.log(reason);
        });

        $(".content").empty().append(handleiding);
    }

    /**
     * Show which language is active
     *
     * @param  lang1 Active language
     * @param  lang2 Non-active language
     * @return void
     */
    function translateGuide(lang1, lang2) {
        $(lang1).addClass("activeTranslation");
        $(lang2).removeClass("activeTranslation");
    }

    /**
     * Change guide, load from views/handleiding/ directory
     *
     * @param  filename Filename which you'd like to show
     * @return Data of filename
     */
    function changeGuide(filename) {
        $(".handleiding-text").load("views/handleiding/" + filename, function(responseTxt, statusTxt, xhr){
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
