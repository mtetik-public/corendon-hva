function userheaderController() {
    //Reference to our loaded view
    var userheaderView;

    function initialize() {
        $.get("views/userheader.html")
            .done(setup)
            .fail(error);
    }

    //Called when the userheader.html has been loaded
    function setup(data) {

        //Load the userheader-content into memory
        userheaderView = $(data);

        // Get current logged in user
        databaseManager
            .query("SELECT * FROM gebruiker WHERE idGebruiker = ? LIMIT 1", [getCurrentLoggedInUserID()])
            .done(function (data) {
                // If profile picture exists
                if(data[0].foto) {
                    var string = data[0].foto;
                    var string1 = string.split(/,(.+)/)[0];
                    var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                    var foto = string1 + ',' + string2;

                    // Set navbar profile image to user's profile picture
                    $("#profile-image").attr("src", foto);
                }
            })
            .fail(function (reason) {
                console.log(reason.responseText);
            });

        //Find all anchors and register the click-event
        userheaderView.find("a").on("click", handleClickMenuItem);

        //Empty the sidebar-div and add the resulting view to the page
        $(".header").empty().append(userheaderView);
        $(".header .header-username").prepend(getCurrentLoggedInUser());
    }

    function handleClickMenuItem() {
        //Get the data-controller from the clicked element (this)
        var controller = $(this).attr("data-controller");

        //Pass the action to a new function for further processing
        loadController(controller);

        //Return false to prevent reloading the page
        return false;
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load the navbar!");
    }

    //Run the initialize function to kick things off
    initialize();
}
