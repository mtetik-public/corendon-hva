/**
 * @author Daan Molendijk
 * De functie is het weergeven van de personen met dezelfde interesses als jij in een tabel.
 */

function matchesLijstController() {
    var matchesLijstView;

    function initialize() {
        $.get("views/matches-lijst.html")
            .done(setup)
            .fail(errorMessage);
    }

    function setup(data) {
        matchesLijstView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="matches-lijst"]').addClass('active');

        var matches = [];
        var sent = [];
        var received = [];

        // Selecteer alle huidige interesses
        databaseManager.query(
            "SELECT naam FROM interesse I INNER JOIN gebruiker G ON I.idGebruiker = G.idGebruiker WHERE G.idGebruiker = ?",
            [getCurrentLoggedInUserID()]
        ).done(function (data) {
            // Per interesse, ga langs bij elke gebruiker met de zelfde interesse
            $.each(data, function(key, value){
                databaseManager.query(
                    "SELECT G.idGebruiker, G.gebruikersnaam, G.voornaam, G.achternaam, G.woonplaats, G.woonplaats_actief FROM interesse I INNER JOIN gebruiker G ON I.idGebruiker = G.idGebruiker WHERE G.idGebruiker <> ? AND I.naam LIKE ?",
                    [getCurrentLoggedInUserID(), '%' + value.naam + '%']
                ).done(function (data) {
                    // Voor elke match, voeg de gebruiker(s) toe aan de table
                    $.each(data, function(key, value){
                        if (matches.includes(value.idGebruiker) || sent.includes(value.idGebruiker) || received.includes(value.idGebruiker)) {
                            return;
                        }
                        $('table tbody').append(
                            '<tr>'
                            + '<td>' + value.gebruikersnaam + '</td>'
                            + '<td>' + (value.voornaam ? value.voornaam : " - ") + '</td>'
                            + '<td>' + (value.achternaam ? value.achternaam : " - ") + '</td>'
                            + '<td>' + (value.woonplaats_actief == 1 ? value.woonplaats : "Afgeschermd") + '</td>'
                            + '<td><input class="btn button-verzoek-sturen" data-userid="' + value.idGebruiker + '" type="button" value="Stuur verzoek!"></td>'
                            + '</tr>'
                        );

                        matches.push(value.idGebruiker);
                    });
                }).fail(function (reason) {
                    console.log(reason);
                });
            });

            databaseManager.query("SELECT idGebruiker2, idGebruiker1, idStatus FROM verzoek WHERE idGebruiker1 = ? OR idGebruiker2 = ? OR idStatus = ?;", [getCurrentLoggedInUserID(), getCurrentLoggedInUserID(), 3])
            .done(function(verzoek){
                /*
                    Hier voegt hij in een array alle gebruikers die al een verzoek naar een persoon hebben gestuurd en
                    de persoon die het verzoek heeft gekregen
                 */
                $.each(verzoek, function(key, value) {
                    sent.push(value.idGebruiker2);
                    received.push(value.idGebruiker1);

                    /*
                        Hier wordt er gekeken of de idStatus = 3 (het verzoek is geweigerd) is en dan worden de twee personen
                        weer toegevoegd aan de tabel zodat ze elkaar weer kunnen toevoegen als ze dat willen.
                    */
                    if (value.idStatus === 3) {
                        var index1 = sent.indexOf(value.idGebruiker2);
                        var index2 = received.indexOf(value.idGebruiker1);

                        sent.splice(index1);
                        received.splice(index2);

                        // Verwijder de row of data waar de idstatus gelijk is aan 3.
                        databaseManager.query("DELETE FROM verzoek WHERE idStatus = 3")
                            .done(function(data) {
                                // on Success
                            }).fail(function(reason) {
                                console.log(reason);
                            })
                    }
                });

                // Hier wordt de klik functie van de verzoek sturen knop geregeld.
                $('table tbody tr .button-verzoek-sturen').click(function () {
                    $('#match-info-box').html('Verzoek gestuurd!').show().fadeOut(1000);
                    $(this).parent().parent().hide();

                    //Pagina wordt herladen.
                    setTimeout(function (){
                        location.reload();
                    }, 500);

                    // Hier worden de gegevens van de geklikte persoon en de ingelogde persoon als query uitgevoerd.
                    databaseManager.query("INSERT INTO verzoek(idGebruiker1, idGebruiker2, idStatus, datum) VALUES(?, ?, ?, NOW());", [getCurrentLoggedInUserID(), $(this).data("userid"), 1])
                        .done(function (data) {
                            // on Success
                        }).fail(function (reason) {
                            console.log(reason);
                        });
                });

                // Table wordt ingeladen zodra webpagina ready is
                // Aantal woorden van dataTable zijn vertaald in het Nederlands
                $(function(){
                    loadDataTable();
                });

            }).fail(function(reason){
                console.log(reason);
            });
        }).fail(function (reason) {
            console.log(reason);
        });

        $(".content").empty().append(matchesLijstView);
    }

    function loadDataTable() {
        $('#example').DataTable({
            "lengthMenu": [[10, 25, 50], [10, 25, 50]],
            "iDisplayLength": 25,
            "language": {
                "emptyTable": "Er zijn geen mensen met dezelfde interesses",
                "search":         "Zoeken:",
                "zeroRecords":    "Zoekterm heeft geen resultaten weergeven",
                "infoEmpty": "0 tot 0 van 0 vermeldingen weergegeven",
                "lengthMenu":     "Aantal: _MENU_ ",
                "paginate": {
                    "first": "Eerste",
                    "last": "Laatste",
                    "next": "Volgende",
                    "previous": "Vorige"
                }
            }
        });
    }

    function errorMessage(){
        $(".content").html("Failed to load!");
    }

    initialize();
}
