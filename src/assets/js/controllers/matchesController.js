/**
 * @author Daan Molendijk
 * De functie is het laden van de verzoekpagina en het weergeven van een verzoek.
 */

function matchesController() {
    var matchesView;

    function initialize() {
        $.get("views/matches.html")
            .done(setup)
            .fail(errorMessage);
    }

    function setup(data) {
        matchesView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="matches"]').addClass('active');

        matchesView.find(".name").html(session.get("username"));

        $(".content").empty().append(matchesView);

        databaseManager.query("SELECT * FROM verzoek INNER JOIN gebruiker ON gebruiker.idGebruiker = verzoek.idGebruiker1 WHERE idGebruiker2 = ? AND idStatus = ?", [getCurrentLoggedInUserID(), 1] )
            .done(function(data) {
                //Gecheckt of er geen data is in de tabel voor de persoon die is ingelogd.
                if(data.length == 0) {
                    $('.gesprekken-box-personen-matches').html("<h3 style='text-align: center;'>Er zijn momenteel geen verzoeken...</h3>");
                }

                $.each(data, function(key, value) {
                    //Hier wordt de geboortedatum van de persoon omgezet, zodat het weergeven kan worden in de personen-box.
                    var datum = new Date(value.geboortedatum);
                    var jaar = datum.getFullYear();
                    var maand = datum.getMonth() + 1;
                    var dag = datum.getDate();

                    //Profielfoto's worden hier goed weergeven bij de verzoeken.
                    var string = value.foto;
                    var string1 = string.split(/,(.+)/)[0];
                    var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                    var foto = string1 + ',' + string2;
                    //Hier wordt de persoon-box gegevens geladen en het accepteren en weigeren knopje.
                    $('.gesprekken-box-personen-matches').append(
                        '<div class="gesprek">'
                            + '<img id="foto-verzoek-persoon" src="' + foto + '">'
                                + '<p class="naam-gesprek-verzoek">'
                                    + (value.voornaam && value.tussenvoegsel && value.achternaam ? value.voornaam + " " + value.tussenvoegsel + " " + value.achternaam : value.gebruikersnaam)
                                    + "<br>"
                                    + "<small class='userDetail'><i class='fas fa-map-marker-alt'></i> " + (value.woonplaats_actief == 1 ? (value.woonplaats.length > 0 ? value.woonplaats : "Woonplaats onbekend") : "Woonplaats afgeschermd") + "</small><br>"
                                    + "<small class='userDetail'><i class='fas fa-birthday-cake'></i> " + (value.leeftijd_actief == 1 ? (value.geboortedatum.length > 0 ? dag + "/" + maand + "/" + jaar : "Geboortedatum onbekend") : "Geboortedatum afgeschermd") + "</small>"
                                + '</p>'
                            + '<img class="foto-denied" data-user="' + value.idGebruiker2 + '" src="https://vignette.wikia.nocookie.net/theloudhouse/images/a/a5/X.png/revision/latest?cb=20170917150003">'
                            + '<img class="foto-verzoek" data-user="' + value.idGebruiker2 + '" src="https://vignette.wikia.nocookie.net/okami/images/f/f0/Check_mark.png/revision/latest?cb=20130520141819">'
                        + '</div>'
                    );

                    //Hier wordt de userid opgenomen van de gebruiker die op het accepteren klikt.
                    $(".gesprek .foto-verzoek").click(function() {
                        var userid = value.idGebruiker1;
                        verzoekAccepteren(userid);

                        return false;
                    });

                    //Hier wordt het userid opgenomen van de gebruiker die op weigeren klikt.
                    $(".gesprek .foto-denied").click(function() {
                        var userid = value.idGebruiker1;
                        verzoekWeigeren(userid);

                        return false;
                    });
                });
            })
            .fail(function(reason) {
                $("#query-result").html(reason);
            });
    }

    /*
        Hier wordt als de gebruiker op het accepteren van een verzoek klikt geregeld dat
        het naar de match tabel in de database komt. Ook wordt de tabel verzoeken geupdate.
     */
    function verzoekAccepteren(id) {
        databaseManager.query("UPDATE verzoek SET idStatus = 2 WHERE idGebruiker1 = ? AND idGebruiker2 = ?", [id, getCurrentLoggedInUserID()])
            .done(function (data) {
                databaseManager.query("INSERT INTO fys_is107_2.`match`(idGebruiker1, idGebruiker2, datum) VALUES(?, ?, NOW())", [id, getCurrentLoggedInUserID()])
                    .done(function (match) {

                }).fail(function (reason) {
                        console.log(reason.responseText);
                    });
                $('[data-user="' + id + '"]').parents(".gesprek").hide();
                $('#match-info-box').html('Match geaccepteerd!').show().fadeOut(1500);

                //Automatisch herlaad de pagina.
                setTimeout(function (){
                    location.reload();
                }, 500)
            })
            .fail(function (reason) {
                console.log(reason.responseText);
            });
    }

    /*
        Hier wordt als de gebruiker op het weigeren van een verzoek klikt geregeld dat
        het verzoek wordt aangepast en terug in de tabel op de matcheslijst pagina komt.
        Ook wordt de tabel geupdate.
     */
    function verzoekWeigeren(id) {
        databaseManager.query("UPDATE verzoek SET idStatus = 3 WHERE idGebruiker1 = ? AND idGebruiker2 = ?", [id, getCurrentLoggedInUserID()])
            .done(function (data) {
                $('[data-user="' + id + '"]').parents(".gesprek").hide();
                $('#match-info-box').html('Match verwijderd').show().fadeOut(1500);

                //Automatische herlaad de pagina.
                setTimeout(function (){
                    location.reload();
                }, 500)
            })
            .fail(function (reason) {
                console.log(reason.responseText);
            });
    }

    function errorMessage() {
        $(".content").html("Failed to load!");
    }

    initialize();

}
