function profileController() {
    //Reference to our loaded view
    var profileView;

    function initialize() {
        $.get("views/profile.html")
            .done(setup)
            .fail(error);
    }

    /**
     * Check whether birthdate is legit
     *
     * @param  data Given date
     * @return boolean whether data is legit
     */
    function checkGeboortedatum(data) {
        var datum = new Date(data);
        var jaar = datum.getFullYear();
        var maand = datum.getMonth() + 1;
        var dag = datum.getDate();

        if(jaar >= 1900) {
            if(maand > 0 && maand <= 12) {
                if(dag > 0 && dag <= 31) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Slice given birthdate
     *
     * @param  data Slice given date (from DB)
     * @return YYYY-MM-DD
     */
    function getGeboortedatum(data) {
        var datum = new Date(data);
        var jaar = datum.getFullYear();
        var maand = datum.getMonth() + 1;
        var dag = datum.getDate();

        return jaar + "-" + ("0" + maand).slice(-2) + "-" + ("0" + dag).slice(-2);
    }

    function setup(data) {
        profileView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="profile"]').addClass('active');

        profileView.find("#btngegevenswijzigen").on("click", function() {
            showInputs();

            return false;
        });

        profileView.find("#btngegevensopslaan").on("click", function() {
            gegevensOpslaan();

            return false;
        });

        profileView.find("#woonplaatsIcon, #geboortedatumIcon").on("click", function() {
            var fieldName = $(this).data('field');

            toggleStatus(fieldName);
        });

        // Na het klikken op btnbestemming komt de pop up van bestemming te voorschijn.
        profileView.find("#btnbestemming").on("click", function() {
            handleBestemmingPopup();


            return false;
        });

        profileView.find("#btninteresse").on("click", function() {
            handleInteressePopup();

            return false;
        });

        profileView.find("#btnafbeelding").on("click", function() {
            handleAfbeeldingPopup();

            return false;
        });

        profileView.find("#afbeeldingsFile").on("change", function() {
           handleChangeAfbeeldingPopup();

           return false;
        });

        // na het klikke op de close button dan sluit de pop-up/
        profileView.find(".close").on("click", function() {
            closePopup($(this).data('popup'));
        });

        $(".content").empty().append(profileView);

        // Haal alles gegevens uit gebruiker van de current user.
        databaseManager
            .query("SELECT * FROM gebruiker WHERE idGebruiker = ? LIMIT 1", [getCurrentLoggedInUserID()])
            .done(function (data) {
                $.each(data, function (key, value) {
                    if (value.gebruikersnaam) {
                        $("#gebruikersnaam").html("<b>" + value.gebruikersnaam + "</b>");
                    }

                    (value.gebruikersnaam ? $("#gebruikersnaam").html("<b>" + value.gebruikersnaam + "</b>") : $("#voornaam").html("_________"));

                    (value.voornaam ? $("#voornaam").html("<b>" + value.voornaam + "</b>") : $("#voornaam").html("_________"));

                    (value.tussenvoegsel ? $("#tussenvoegsel").html("<b>" + value.tussenvoegsel + "</b>") : $("#tussenvoegsel").html("_________"));

                    (value.achternaam ? $("#achternaam").html("<b>" + value.achternaam + "</b>") : $("#achternaam").html("_________"));

                    (value.email ? $("#email").html("<b>" + value.email + "</b>") : $("#email").html("_________"));

                    (value.woonplaats ? $("#woonplaats").html("<b>" + value.woonplaats + "</b>") : $("#woonplaats").html("_________"));

                    (checkGeboortedatum(value.geboortedatum) ? $("#geboortedatum").html("<b>" + getGeboortedatum(value.geboortedatum) + "</b>") : $("#geboortedatum").html("_________"));

                    // als woonplaatsacttief op 0 staat dan komt er een sluitende oog te zien. als het op 1 staat dan
                    //laat een opende oog zien.
                    if (value.woonplaats) {

                        if(value.woonplaats_actief == 0){
                            $("#woonplaatsIcon").html('<span><i title="hidden" class="fas fa-eye-slash"></i></span>');
                        }

                        if(value.woonplaats_actief == 1){
                            $("#woonplaatsIcon").html('<i title="visible" class="fas fa-eye"></i>');
                        }
                    }

                    if (value.geboortedatum) {
                        if(checkGeboortedatum(value.geboortedatum)){
                            if(value.leeftijd_actief == 0){
                                $("#geboortedatumIcon").html('<i title="hidden" class="fas fa-eye-slash"></i>');
                            }

                            if(value.leeftijd_actief == 1){
                                $("#geboortedatumIcon").html('<i title="visible" class="fas fa-eye"></i>');
                            }
                        }
                    }
                });
            })
            .fail(function (reason) {
                console.log(reason);
            });
        // Haalt alleen de naam op van de interesse.
        databaseManager
            .query("SELECT naam FROM interesse WHERE idGebruiker = ?", [getCurrentLoggedInUserID()])
            .done(function (data) {
                // Kijk of data bestaat
                if(data.length > 0) {
                    $.each(data, function (key, value) {
                        // als veld naam ingevuld is dan appand hij de naam in een class. anders laat hij niks zien.
                        if (value.naam) {
                            $("#interessestekst").append("<p class='intresseTxt'>" + value.naam + "</p>");
                        } else {
                            $("#interessestekst").empty();
                        }
                    });
                } else {
                    $("#interessestekst").append("<p class='niet-aanwezig'>U heeft nog geen interesse toegevoegd.</p>");
                }
            })
            .fail(function (reason) {
                $("#name").html(reason);
            });

        databaseManager
            .query("SELECT plaats, land, aankomst, vertrek FROM bestemming WHERE idGebruiker = ?", [getCurrentLoggedInUserID()])
            .done(function (data) {
                // Kijk of data bestaat
                if(data.length > 0) {
                    $.each(data, function (key, value) {
                        if (value.plaats, value.land, value.aankomst, value.vertrek) {
                            var aankomst = value.aankomst;
                            // hij pakt alles voor de letter "t"
                            aankomst = aankomst.substring(0, aankomst.indexOf('T'));
                            var vertrek = value.vertrek;
                            vertrek = vertrek.substring(0, vertrek.indexOf('T'));
                            $("#plaatstekst").append("<p class='plaatsTxt'>Plaats: " + value.plaats + "</p>")
                                            .append("<p class='landTxt'>Land: " + value.land + "</p>")
                                            .append("<p class='aankomstTxt'>Aankomst: " + aankomst + "</p>")
                                            .append("<p class='vertrekTxt'>Vertrek: " + vertrek + "</p>");
                        } else {
                            $("#plaatstekst").empty();
                        }
                    });
                } else {
                    $("#plaatstekst").append("<p class='niet-aanwezig'>U heeft nog geen bestemmingen toegevoegd.</p>");
                }
            })
            .fail(function (reason) {
                console.log(reason);
            });

        databaseManager
            .query("SELECT foto FROM gebruiker WHERE idGebruiker = ? LIMIT 1", [getCurrentLoggedInUserID()])
            .done(function (data) {
                $.each(data, function (key, value) {
                    if (value.foto) {
                        //Haalt alle commas uit de hele lange base64 string
                      var string = value.foto;
                      var string1 = string.split(/,(.+)/)[0];
                      var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                      var foto = string1 + ',' + string2;
                        var img = $("<img>")
                            .addClass("profile-image")
                            .attr("src", foto);

                        $("#profileimg").append(img);
                    }
                });
            })
            .fail(function (reason) {
                $("#name").html(reason);
            });
    }

    function showInputs() {
        // na het klikken op gegevens wijzigen komen de gegevens in een input.
        databaseManager
            .query("SELECT * FROM gebruiker WHERE idGebruiker = ?", [getCurrentLoggedInUserID()])
            .done(function (data) {
                $("#gebruikersnaam").empty().html('<input id="gebruikersnaamInput" class="gegevensInput" type="text" value="' + data[0].gebruikersnaam + '" disabled="disabled"/>');
                $("#voornaam").empty().html('<input id="voornaamInput" class="gegevensInput" type="text" value="' + data[0].voornaam + '" />');
                $("#tussenvoegsel").empty().html('<input id="tussenvoegselInput" class="gegevensInput" type="text" value="' + data[0].tussenvoegsel + '" />');
                $("#achternaam").empty().html('<input id="achternaamInput" class="gegevensInput" type="text" value="' + data[0].achternaam + '" />');
                $("#email").empty().html('<input id="emailInput" class="gegevensInput" type="email" value="' + data[0].email + '" />');
                $("#woonplaats").empty().html('<input id="woonplaatsInput" class="gegevensInput" type="text" value="' + data[0].woonplaats + '" />');
                $("#geboortedatum").empty().html('<input id="geboortedatumInput" class="gegevensInput" type="date" min="1900-01-01" max="2000-01-01" value="' + getGeboortedatum(data[0].geboortedatum) + '" />');

                $("#geboortedatumIcon, #woonplaatsIcon").css("display", "none");

                $("#btngegevenswijzigen").css("display", "none");
                $("#btngegevensopslaan").css("display", "block");
            })
            .fail(function (reason) {
                console.log(reason.responseText)
            });

    }

    function gegevensOpslaan() {
        var gebruikersnaam = $("#gebruikersnaamInput");
        var voornaam = $("#voornaamInput");
        var tussenvoegsel = $("#tussenvoegselInput");
        var achternaam = $("#achternaamInput");
        var email = $("#emailInput");
        var woonplaats = $("#woonplaatsInput");
        // de regex voor een correcte emailadres.
        var regex_email = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var geboortedatum = $("#geboortedatumInput").val();
        // de regex voor een correcte woonplaats
        var regex_woonplaats = /[^a-zA-Z]/;

        if (!geboortedatum) {
            geboortedatum = "0000-00-00";
        }

        $("#btngegevensopslaan").html('<i class="fa fa-spinner fa-spin"></i> Opslaan');
        // na het klikken op opslaan dan update hij de gegevens van de gebruiker
        setTimeout(function () {
            databaseManager
                .query(
                    "UPDATE gebruiker SET gebruikersnaam = ?, voornaam = ?, tussenvoegsel = ?, achternaam = ?, geboortedatum = ?, woonplaats = ?, email = ? WHERE idGebruiker = ?",
                    [gebruikersnaam.val(), voornaam.val(), tussenvoegsel.val(), achternaam.val(), geboortedatum, woonplaats.val(), email.val(), getCurrentLoggedInUserID()]
                )
                .done(function (data) {
                    // na het updaten dan pakt hij de gewijzigde gegevens.
                    databaseManager
                        .query("SELECT * FROM gebruiker WHERE idGebruiker = ?", [getCurrentLoggedInUserID()])
                        .done(function (data) {
                            if(regex_woonplaats.test($("#woonplaatsInput").val())) {
                                alert("Woonplaats mag geen nummer/karakters bevatten!");

                                $("#btngegevensopslaan").html('Opslaan');
                                // als de emailinput niet voldoet aan de email regex dan komt er een foutmelding.
                          } else if (!regex_email.test($("#emailInput").val())){
                                alert("Voer een geldige email in!");

                                $("#btngegevensopslaan").html('Opslaan');
                            }else {
                                (data[0].gebruikersnaam ? $("#gebruikersnaam").empty().html("<b>" + data[0].gebruikersnaam + "</b>") : $("#gebruikersnaam").empty().html("_________"));
                                (data[0].voornaam ? $("#voornaam").empty().html("<b>" + data[0].voornaam + "</b>") : $("#voornaam").empty().html("_________"));
                                (data[0].tussenvoegsel ? $("#tussenvoegsel").empty().html("<b>" + data[0].tussenvoegsel + "</b>") : $("#tussenvoegsel").empty().html("_________"));
                                (data[0].achternaam ? $("#achternaam").empty().html("<b>" + data[0].achternaam + "</b>") : $("#achternaam").empty().html("_________"));
                                (data[0].email ? $("#email").empty().html("<b>" + data[0].email + "</b>") : $("#email").empty().html("_________"));

                                if (data[0].woonplaats) {
                                    $("#woonplaats").empty().html("<b>" + data[0].woonplaats + "</b>");
                                    $("#woonplaatsIcon").css("display", "block");
                                } else {
                                    $("#woonplaatsIcon").css("display", "none");
                                    $("#woonplaats").empty().html("_________");
                                }
                                //als het beld geboortedatum ingevuld is dan laat hij de icon zien. anders zie je een
                                // lange zwarte streep.
                                if (checkGeboortedatum(data[0].geboortedatum)) {
                                    $("#geboortedatum").empty().html("<b>" + getGeboortedatum(data[0].geboortedatum) + "</b>");
                                    $("#geboortedatumIcon").css("display", "block");
                                } else {
                                    $("#geboortedatum").empty().html("_________");
                                    $("#geboortedatumIcon").css("display", "none");
                                }

                                $("#btngegevensopslaan").css("display", "none");
                                $("#btngegevenswijzigen").css("display", "block");
                                $("#btngegevensopslaan").html("Opslaan");
                            }
                        })
                        .fail(function (reason) {
                            console.log(reason.responseText)
                        });
                })
                .fail(function (reason) {
                    console.log(reason.responseText)
                });
        }, 1500);
    }

    function closePopup(popup) {
        $("#" + popup).css('display', 'none');
    }

    function toggleStatus(name) {
        databaseManager
            .query("SELECT * FROM gebruiker WHERE idGebruiker = ? LIMIT 1", [getCurrentLoggedInUserID()])
            .done(function (data) {
                if(data.length > 0){
                    var statusValue = data[0][name];
                    // als de status van value update naar 1 dan ziet u de icon met de oog open. anders de icon met de
                    //gesloten oog.
                    if(statusValue == 0) {
                        databaseManager
                            .query("UPDATE gebruiker SET " + name + " = ? WHERE idGebruiker = ? LIMIT 1", [1, getCurrentLoggedInUserID()])
                            .done(function (data) {
                                $('[data-field="' + name + '"]').html('<i title="visible" class="fas fa-eye"></i>');
                            })
                            .fail(function (reason) {
                                console.log(reason);
                            });
                    }

                    if(statusValue == 1) {
                        databaseManager
                            .query("UPDATE gebruiker SET " + name + " = ? WHERE idGebruiker = ? LIMIT 1", [0, getCurrentLoggedInUserID()])
                            .done(function (data) {
                                $('[data-field="' + name + '"]').html('<i title="hidden" class="fas fa-eye-slash"></i>');
                            })
                            .fail(function (reason) {
                                console.log(reason);
                            });
                    }
                }
            })
            .fail(function (reason) {
                console.log(reason);
            });
    }

    function handleInteressePopup() {
        var interesse = $(".interesse");
        var id = getCurrentLoggedInUserID();

        $("#interessePopup").css('display', 'block');

        $(".btn-green-profile").on("click", function() {
            //als de inplut leeg is dan komt de class "inputerror" te zien
            if(!interesse.val()){
                interesse.addClass("inputError");
            }else{
                interesse.removeClass("inputError");
            }

            if(interesse.val()) {
                $(interesse).prop('disabled', true);
                $('.btn-green-profile').html('<i class="fa fa-spinner fa-spin"></i> Interesse opslaan');
                 // na het klikken op opslaan dan insert hij het in de database.
                databaseManager
                    .query("INSERT INTO interesse(naam, idGebruiker) VALUES (?, ?)", [interesse.val(), id])
                    .done(function (data) {
                        setTimeout(function () {
                            $(interesse).prop('disabled', false);

                            $('.interesse').val('');
                            setTimeout(function () {
                                $('.btn-green-profile').html('Interesse opslaan');
                            }, 1000);

                            setTimeout(function () {
                                location.reload();
                            }, 1000);

                            $('.btn-green-profile').hide().html('<i class="fa fa-check-circle"></i> Interesse opgeslagen').fadeIn('slow');
                        }, 1000);
                    })
                    .fail(function (reason) {
                        console.log(reason);
                    });
            }
        });
    }

    function handleBestemmingPopup() {
        $("#bestemmingPopup").css('display', 'block');
        // datepicker met de format
        $("#startDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date(),
            maxDate: '+2y',
            onSelect: function(date){

                var selectedDate = new Date(date);
                var msecsInADay = 86400000;
                var endDate = new Date(selectedDate.getTime() + msecsInADay);

                //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
                $("#endDatePicker").datepicker( "option", "minDate", endDate );
                $("#endDatePicker").datepicker( "option", "maxDate", '+2y' );

            }
        });

        $("#endDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true
        });

        $(".bestemmingformBtn").on("click", function() {
            var plaats = $('#bestemmingplaats');
            var land = $('#bestemmingland');
            var aankomst = $('#startDatePicker');
            var vertrek = $('#endDatePicker');

            if(!plaats.val()){
                plaats.addClass("inputError");
            }else{
                plaats.removeClass("inputError");
            }

            if(!land.val()){
                land.addClass("inputError");
            }else{
                land.removeClass("inputError");
            }

            if(!aankomst.val()){
                aankomst.addClass("inputError");
            }else{
                aankomst.removeClass("inputError");
            }

            if(!vertrek.val()){
                vertrek.addClass("inputError");
            }else{
                vertrek.removeClass("inputError");
            }
            // als alles is ingevuld dan insert hij de gegevens van de bestemming
            if(plaats.val() && land.val() && aankomst.val() && vertrek.val()) {
                databaseManager.query(
                    "INSERT INTO bestemming (plaats, land, aankomst, vertrek, idGebruiker)"
                    + " VALUES(?, ?, ?, ?, ?)",
                    [plaats.val(), land.val(), aankomst.val(), vertrek.val(), getCurrentLoggedInUserID()]
                ).done(function(data) {
                    $('.bestemmingformBtn').html('<i class="fa fa-spinner fa-spin"></i> Bestemming opslaan');
                    setTimeout(function(){
                        $('.bestemmingformBtn').hide().html('<i class="fa fa-check-circle"></i> Bestemming opgeslagen').fadeIn('slow');
                        $('#bestemmingplaats, #bestemmingland, #startDatePicker, #endDatePicker').val('');
                        setTimeout(function(){
                            $('.profile.form-group').fadeIn();
                            $('.bestemmingformBtn').html('opslaan');
                        }, 1500);
                    }, 1000);
                    setTimeout(function(){
                        location.reload();
                    }, 1500);
                }).fail(function(reason) {
                    console.log(reason);
                });
            }
        });

        $(function() {
            $( ".datepicker" ).datepicker();
        });
    }

    function handleChangeAfbeeldingPopup() {
        // laat de previeuw zien van de image
        var preview = document.querySelector('#afbeeldingPopup > div > div.modal-body > div > img');

        handleImagePreview(function(data) {
            preview.src = data;
        });
    }

    function handleImagePreview(callback) {
        //opent de filereader, zodat de gebruiker een image kan uitkiezen
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            callback(reader.result);
        }, false);

        reader.readAsDataURL(file);
    }

     function handleAfbeeldingPopup() {
         $("#afbeeldingPopup").css('display', 'block');

         $(".afbeeldingBtn").on("click", function() {

             var preview = document.querySelector('#afbeeldingPopup > div > div.modal-body > div > img');

             // if user has chosen an image, dan update hij het veld in het database
             if(preview.src.length > 50){
                 databaseManager
                     .query("UPDATE gebruiker SET foto = ? WHERE idGebruiker = ? LIMIT 1", [preview.src, getCurrentLoggedInUserID()])
                     .done(function (data) {
                         $('.afbeeldingBtn').html('<i class="fa fa-spinner fa-spin"></i> Afbeelding opslaan');
                         setTimeout(function(){
                             $('.afbeeldingBtn').hide().html('<i class="fa fa-check-circle"></i> Afbeelding opgeslagen').fadeIn('slow');
                             setTimeout(function(){
                                 $('.afbeeldingBtn').html('opslaan');
                             }, 1500);
                         }, 1000);
                         setTimeout(function(){
                             location.reload();
                         }, 1500);
                     })
                     .fail(function (reason) {
                         console.log(reason);
                     });
             }else{
                 $('.afbeeldingBtn').html("Selecteer een afbeelding");
                 setTimeout(function(){
                     $('.afbeeldingBtn').html("Opslaan");
                 }, 1500);
             }
         });
     }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
