function guestHandleidingController() {
    var handleiding;

    function initialize() {
        $.get("views/guestHandleiding.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        handleiding = $(data);

        $('#navigation-list li a').removeClass('active');

        // Timeout so the data can be loaded succesfully
        setTimeout(function() {
            changeGuide("guestGuideNL.html");
        }, 500);

        // On NL translate click, set guest guide to NL
        // Change language to NL
        handleiding.find("#nlTranslation").on("click", function(){
            translateGuide("#nlTranslation", "#enTranslation");

            changeGuide("guestGuideNL.html");
        });

        // On EN translate click, set guest guide to EN
        // Change language to EN
        handleiding.find("#enTranslation").on("click", function(){
            translateGuide("#enTranslation", "#nlTranslation");

            changeGuide("guestGuideEN.html");
        });

        $(".content").empty().append(handleiding);
    }

    /**
     * Show which language is active
     *
     * @param  lang1 Active language
     * @param  lang2 Non-active language
     * @return void
     */
    function translateGuide(lang1, lang2) {
        $(lang1).addClass("activeTranslation");
        $(lang2).removeClass("activeTranslation");
    }

    /**
     * Change guide, load from views/handleiding/ directory
     *
     * @param  filename Filename which you'd like to show
     * @return Data of filename
     */
    function changeGuide(filename) {
        $(".handleiding-text").load("views/handleiding/" + filename, function(responseTxt, statusTxt, xhr){
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
