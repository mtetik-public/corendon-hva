function adminheaderController() {
    //Reference to our loaded view
    var sidebarView;
    
    function initialize() {
        $.get("views/adminheader.html")
            .done(setup)
            .fail(error);
    }

    //Called when the sidebar.html has been loaded
    function setup(data) {
        //Load the sidebar-content into memory
        sidebarView = $(data);

        //Find all anchors and register the click-event
        sidebarView.find("a").on("click", handleClickMenuItem);

        //Empty the sidebar-div and add the resulting view to the page
        $(".header").empty().append(sidebarView);
        $(".header .header-username").append(getCurrentLoggedInUser());
    }

    function handleClickMenuItem() {
        //Get the data-controller from the clicked element (this)
        var controller = $(this).attr("data-controller");

        //Pass the action to a new function for further processing
        loadController(controller);

        //Return false to prevent reloading the page
        return false;
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load the navbar!");
    }

    //Run the initialize function to kick things off
    initialize();
}
