function chatController() {
    var chatView;
    var messages = [];

    function initialize() {
        $.get("views/chat.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        chatView = $(data);

        $('#navigation-list li a').removeClass('active');

        $(".content").empty().append(chatView);
        // Get all user data from the concerning user
        databaseManager.query("SELECT * FROM gebruiker WHERE idGebruiker = ? ", [getUserChat()] )
            .done(function (data) {
                // Split the "foto" string in 2. Remove all commas from the second part of the string and put the strings back together
                var string = data[0].foto;
                var string1 = string.split(/,(.+)/)[0];
                var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                var foto = string1 + ',' + string2;
              $(".naam-gesprek").text(data[0].gebruikersnaam);
              $("#foto").attr("src", foto);
            })
            .fail(function(reason){
              console.log(reason);
            });
            // Show the chat
            ShowChat();
        // On enter send the message the user typed
        chatView.find('.inputChat').keypress(function(e) {
            if(e.which == 13) {
                if($('.inputChat').val()){
                    sendChatMessage();
                }else{
                    $('.inputChat').css('background-color', '#D81E05');
                    setTimeout(function() {
                        $('.inputChat').css('background-color', '#fff');
                    }, 1000);
                }
            }
        });

        return false;
    }
    // Get all messages from the concerning chat.
    function ShowChat() {
        databaseManager.query("SELECT * FROM chat INNER JOIN gebruiker ON idGebruiker1 = idGebruiker WHERE idGebruiker1 = ? AND idGebruiker2 = ? OR idGebruiker1 = ? AND idGebruiker2 = ? ORDER BY timestamp ASC;", [getCurrentLoggedInUserID(), getUserChat(), getUserChat(), getCurrentLoggedInUserID()])
          .done(function(data){
            $.each(data, function(key, value){
                var datum = new Date(value.timestamp);
                var dd = datum.getDate();
                var mm = datum.getMonth() + 1;
                var yyyy = datum.getFullYear();

                var uur = (datum.getHours()<10?'0':'') + (datum.getHours() - 1);
                var minuten = (datum.getMinutes()<10?'0':'') + datum.getMinutes();
                var datum = dd + '/' + mm + '/' + yyyy;
                var tijdstip = uur + ":" + minuten;

                // Split the "foto" string in 2. Remove all commas from the second part of the string and put the strings back together
                var string = value.foto;
                var string1 = string.split(/,(.+)/)[0];
                var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                var foto = string1 + ',' + string2;
                // Check if the message is already shown. If the message is already shown go to the next message
                if (messages.includes(value.idChat)) {
                    return;
                } else {
                    // If the logged in user sent the message give the message its own styling
                    if(value.idGebruiker1 == getCurrentLoggedInUserID()){
                        // Add the message that is sent to the messages array
                      messages.push(value.idChat);
                        $('.chat-box .chat .chat-messages').append(
                        '<div class="chatContainer darker">' +
                        '<img src="' + foto + '" alt="Avatar" class="right">' +
                        '<p>' + value.bericht + '</p>' +
                        '<span class="time-left">' + datum + '<br>' + tijdstip + '</span>' +
                        '</div>');
                        // If the logged in user sent the message give the message its own styling
                    }else {
                        // Add the message that is sent to the messages array
                        messages.push(value.idChat);
                        $('.chat-box .chat .chat-messages').append(
                        '<div class="chatContainer">' +
                        '<img src="' + foto + '" alt="Avatar">' +
                        '<p>' + value.bericht + '</p>' +
                        '<span class="time-left">' + datum + '<br>' + tijdstip + '</span>' +
                        '</div>');
                    }
                }
            });
          })
          .fail(function(reason){
            console.log(reason);
          });
    }
    // Send the message the user sent to the database
    function sendChatMessage() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        var uur = today.getHours();
        var minuten = today.getMinutes();

        var datum = dd + '/' + mm + '/' + yyyy;
        var tijdstip = uur + ":" + minuten;

        var message = $('.inputChat').val();
        databaseManager.query("SELECT * FROM gebruiker WHERE idGebruiker = ? ", [getCurrentLoggedInUserID()] )
        .done(function(data){
            // Split the "foto" string in 2. Remove all commas from the second part of the string and put the strings back together
            var string = data[0].foto;
            var string1 = string.split(/,(.+)/)[0];
            var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

            var foto = string1 + ',' + string2;
          databaseManager.query("INSERT INTO chat (bericht, timestamp, idGebruiker1, idGebruiker2) VALUES (?, NOW(), ?, ?)", [message, getCurrentLoggedInUserID(), getUserChat() ] )
          .done(function(insert_data){
              // Add the message that is sent to the messages array
              messages.push(insert_data.insertId);
            $('.chat-box .chat .chat-messages').append(
                '<div class="chatContainer darker">' +
                '<img src="' + foto + '" alt="Avatar" class="right">' +
                '<p>' + message + '</p>' +
                '<span class="time-left">' + datum + '<br>' + tijdstip + '</span>' +
                '</div>'
            );
          })
          .fail(function(reason) {
            console.log(reason);
          });
        }).fail(function(reason){

        });


        $('.inputChat').val('');
        window.setInterval(function(){
          ShowChat();
          $('.chat-messages').animate({
                scrollTop: $('.chat-messages').prop("scrollHeight")
            }, 1000);
        }, 5000);
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
