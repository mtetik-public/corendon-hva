function contactController() {
    var contactView;

    function initialize() {
        $.get("views/contact.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        contactView = $(data);

        // Set previous nav item to non-active
        $('#navigation-list li a').removeClass('active');
        // Set current nav item to active
        $('[data-controller="contact"]').addClass('active');

        // Check whether user is logged in
        if(isUserLoggedIn()) {
            databaseManager
                // Retrieve user data of current logged in user
                .query("SELECT * FROM gebruiker WHERE idGebruiker = ?", [getCurrentLoggedInUserID()])
                .done(function (data) {
                    // Fill in automatically the contact form
                    // Set firstname of user
                    $('#voornaam').val(data[0].voornaam);

                    // Set middle name of user
                    $('#tussenvoegsel').val(data[0].tussenvoegsel);

                    // Set lastname of user
                    $('#achternaam').val(data[0].achternaam);

                    // Set email of user
                    $('#email').val(data[0].email);
                })
                .fail(function (reason) {
                    console.log(reason.responseText);
                });
        }

        // On contact form send button click
        contactView.find('.contactformVersturenBtn').on('click', function() {
            handleContactForm();

            return false;
        });

        $(".content").empty().append(contactView);
    }

    // Handle validation and send contact form
    function handleContactForm() {

        // Selectors of fields
        var voornaam = $('#voornaam');
        var tussenvoegsel = $('#tussenvoegsel');
        var achternaam = $('#achternaam');
        var email = $('#email');
        var onderwerp = $('#onderwerp');
        var opmerking = $('#opmerking');

        // If firstname field is empty
        if(!voornaam.val()){
            voornaam.addClass("inputError");
        }else{
            voornaam.removeClass("inputError");
        }

        // If lastname field is empty
        if(!achternaam.val()){
            achternaam.addClass("inputError");
        }else{
            achternaam.removeClass("inputError");
        }

        // If email field is empty
        if(!email.val()){
            email.addClass("inputError");
        }else{
            email.removeClass("inputError");
        }

        // If message field is empty
        if(!opmerking.val()){
            opmerking.addClass("inputError");
        }else{
            opmerking.removeClass("inputError");
        }

        // If firstname, lastname, email and message have been filled in
        if(voornaam.val() && achternaam.val() && email.val() && opmerking.val()){
            $.ajax({
                // Handle action before the data has been sent
                beforeSend: function() {
                    $('.contactformVersturenBtn').html('<i class="fa fa-spinner fa-spin"></i> Verzenden');
                }
            })
            .done(function(data) {
                // Insert into contact table on success
                databaseManager.query(
                    "INSERT INTO contact (voornaam, tussenvoegsel, achternaam, email, onderwerp, opmerking, datum)"
                    + " VALUES(?, ?, ?, ?, ?, ?, NOW())",
                    [voornaam.val(), tussenvoegsel.val(), achternaam.val(), email.val(), onderwerp.val(), opmerking.val()]
                )
                .done(function(data) {
                    // Handle some styling actions
                    setTimeout(function(){
                        $('.contactformVersturenBtn').hide().html('<i class="fa fa-check-circle"></i> Bericht verzonden!').fadeIn('slow');
                        $('#voornaam, #achternaam, #email, #opmerking').val('').css('border', '2px solid #d9d9d9');
                        setTimeout(function(){
                            $('.contact.form-group').fadeIn();
                            $('.contactformVersturenBtn').html('Versturen');
                        }, 1500);
                    }, 1000);
                }).fail(function(reason) {
                    console.log(reason);
                });
            }).fail(function(xhr) {
                console.log(xhr.responseText);
            });
        }
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
