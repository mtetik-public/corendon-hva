/**
 * @author Daan Molendijk
 * Functie is het laden van de gesprekken pagina.
 * Hij weergeeft de personen waarmee
 */

function gesprekkenController() {
    var gesprekkenView;

    function initialize() {
        $.get("views/gesprekken.html")
            .done(setup)
            .fail(errorMessage);
    }

    function setup(data) {
        gesprekkenView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="gesprekken"]').addClass('active');

        databaseManager.query("SELECT * FROM fys_is107_2.`match` WHERE idGebruiker2 = ? OR idGebruiker1 = ?", [getCurrentLoggedInUserID(), getCurrentLoggedInUserID()] )
            .done(function (data) {
                if(data.length == 0) {
                    $('.gesprekken-box-personen').html("<h3 style='text-align: center;'>U heeft momenteel geen gesprekken...</h3>");
                }
                //Hier wordt gecheckt wwie ingelogd is zodat de goede namen worden weergeven voor de gesprekken.
                $.each(data, function(matchkey, matchvalue){
                    if (matchvalue.idGebruiker1 == getCurrentLoggedInUserID()) {
                        var gebruiker_id = matchvalue.idGebruiker2;
                    } else if (matchvalue.idGebruiker2 == getCurrentLoggedInUserID()) {
                        var gebruiker_id = matchvalue.idGebruiker1;
                    }
                    databaseManager.query("SELECT * FROM gebruiker WHERE idGebruiker = ?;", [gebruiker_id])
                        .done(function(userdata){
                            $.each(userdata, function(key, value) {
                                //Hier wordt de geboortedatum van de persoon omgezet, zodat het weergeven kan worden in de gesprekken-box.
                                var datum = new Date(value.geboortedatum);
                                var jaar = datum.getFullYear();
                                var maand = datum.getMonth() + 1;
                                var dag = datum.getDate();

                                var string = value.foto;
                                var string1 = string.split(/,(.+)/)[0];
                                var string2 = string.split(/,(.+)/)[1].replace(/,/g, '');

                                var foto = string1 + ',' + string2;
                                //Hier wordt het gepsrek-box aangemaakt met de gegevens van de persoon waarvan het verzoek is geaccepteerd.
                                $('.gesprekken-box-personen').append(
                                    '<div class="' + (value.gebruiker_actief == 1 ? "gesprek" : "gesprek nonActiveUser") + '" data-id="' + value.idGebruiker + '">'
                                        + '<img id="foto-verzoek-persoon" src="' + foto + '">'
                                        + '<p class="naam-gesprek-verzoek">'
                                            + (value.voornaam && value.tussenvoegsel && value.achternaam ? value.voornaam + " " + value.tussenvoegsel + " " + value.achternaam : value.gebruikersnaam)
                                            + "<br>"
                                            + "<small class='userDetail'><i class='fas fa-map-marker-alt'></i> " + (value.woonplaats_actief == 1 ? (value.woonplaats.length > 0 ? value.woonplaats : "Woonplaats onbekend") : "Woonplaats afgeschermd") + "</small><br>"
                                            + "<small class='userDetail'><i class='fas fa-birthday-cake'></i> " + (value.leeftijd_actief == 1 ? (value.geboortedatum.length > 0 ? dag + "/" + maand + "/" + jaar : "Geboortedatum onbekend") : "Geboortedatum afgeschermd") + "</small>"
                                        + '</p>'
                                        + (value.gebruiker_actief == 1 ? '' : '<div class="lockedChat"><span>Gebruiker inactief</span></div>')
                                    + '</div>'
                                );

                                //Al klik je op een gesprek wordt je doorgestuurd naar een chat met de persoon waar je op heb gedrukt.
                                $(".gesprek").click(function() {
                                    session.set("chatUserId", $(this).attr("data-id"));
                                    loadController(CONTROLLER_CHAT);
                                });

                            });
                        })
                        .fail(function(reason){

                        });
                });
            })
            .fail(function (reason) {
                $("#query-result").html(reason);
            });

        $(".content").empty().append(gesprekkenView);
    }

    function errorMessage(){
        $(".content").html("Failed to load!");
    }

    initialize();
}
