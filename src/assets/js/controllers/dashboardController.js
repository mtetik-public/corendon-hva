function dashboardController() {
    var dashboardView;

    function initialize() {
        $.get("views/dashboard.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        dashboardView = $(data);

        $('#navigation-list li a').removeClass('active');
        $('[data-controller="dashboard"]').addClass('active');

        // Check whether user is logged in & check user's role, show data dynamic
        if(isUserLoggedIn()){

            // If user is an admin
            // Load admin environment
            if(getUserRol() == 2){
                loadController(CONTROLLER_BEHEERDER);
            }
            dashboardView.find('.home-registreren').html(
                '<h2>Vind hier je matches!</h2>' +
                '<a href="" class="btn btn-green matchesBtn">Matches</a>'
            )
        }else{
            dashboardView.find('.home-registreren').html(
                '<h2>Registreer nu!</h2>' +
                '<a href="" class="btn btn-green registerBtn">Registreer</a> ' +
                '<a href="" class="btn btn-lightblue handleidingBtn">Handleiding</a>'
            )
        }

        // On matchesBtn click, open 'matches' page
        dashboardView.find('.matchesBtn').on('click', function() {
            loadController(CONTROLLER_MATCHES_LIJST);

            return false;
        });

        // On registerBtn click, open 'register' page
        dashboardView.find('.registerBtn').on('click', function() {
            loadController(CONTROLLER_REGISTER);

            return false;
        });

        // On handleidingBtn click, open guest guide
        dashboardView.find('.handleidingBtn').on('click', function() {
            loadController(CONTROLLER_GUEST_HANDLEIDING);

            return false;
        });

        // On any image click, trigger getBiggerImage(img.id) function
        dashboardView.find('img').on('click', function() {
            getBiggerImage($(this).attr("id"));
        });

        // On close click button, hide image
        dashboardView.find('.close').on('click', function() {
            closeImage();
        });

        $(".content").empty().append(dashboardView);
    }

    /**
     * Make image bigger on click
     *
     * @param  image select image and get the ID attribute of the element
     * @return void
     */
    function getBiggerImage(image) {
        var modal = $("#imgModal");
        var modalContent = $(".modal-content");
        var img = $("#" + image);
        var captionText = $("#caption");

        modal.css("display", "block");
        $("body").css("overflow", "hidden");

        modalContent.attr("src", img.attr("src"));
        captionText.text("Vakantie foto (" + img.attr("id") + ")");
    }

    /**
     * Close image on 'close' click button
     *
     * @return void
     */
    function closeImage() {
        var modal = $("#imgModal");

        modal.css("display", "none");
        $("body").css("overflow", "auto");
    }

    function error() {
        $(".content").html("Failed to load content!");
    }

    initialize();
}
