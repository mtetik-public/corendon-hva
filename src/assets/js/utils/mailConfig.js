function mailConfig() {
    var url = "http://localhost:8081";

    /**
     * Connect to email url
     *
     * @param  varchar newUrl New email url
     * @return void
     */
    function connect(newUrl) {
        url = newUrl;
    }

    /**
     * Send new mail
     *
     * @param  varchar from    Email from
     * @param  varchar to      Email to
     * @param  varchar subject Subject of the mail
     * @param  varchar message Message body of the mail
     * @return void
     */
    function sendMail(from, to, subject, message) {
        var promise = $.Deferred();

        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify({
                from: from,
                to: to,
                subject: subject,
                html: message
            })
        }).done(function(data) {
            promise.resolve(data);
        }).fail(function(xhr) {
            console.log(xhr.responseText);
            if(xhr.status === 400) {
                var data = JSON.parse(xhr.responseText);

                promise.reject(data.reason);
            }
            else {
                promise.reject("Something bad happened, see console.");
            }
        });

        return promise;
    }

    return {
        connect: connect,
        sendMail: sendMail
    }
}
