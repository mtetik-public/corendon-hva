const http = require("http");
const nodemailer = require("nodemailer");
const config = safeRequire("./config.json", "Error: Could not load 'config.json', please make a copy of 'config.template.json'!");

const httpMethodOPTIONS = "OPTIONS";
const httpMethodGET = "GET";
const httpMethodPOST = "POST";

const errorInvalidRequestMethod = "Invalid request method";
const errorInvalidData = "Invalid data";

const defaultHeaders = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Headers": "*"
};

if(http && nodemailer && config) {
    console.log("Starting server...");

    startServer();
}
else {
    console.log("Exiting server due to errors...");
}

function startServer() {
    http
        .createServer(handleRequest)
        .listen(config.server.port, config.server.host);

    console.log(`Listening at ${config.server.host}:${config.server.port}...`);
}

function handleRequest(request, response) {
    try {
        if(request.method === httpMethodOPTIONS || (request.method === httpMethodGET && request.url === "/test")) {
            sendOK(response);

            return;
        }

        if(request.method !== httpMethodPOST) {

            sendBadRequest(response, errorInvalidRequestMethod);

            return;
        }

        handlePOST(request, response);
    }
    catch(e) {
        sendBadRequest(response, e.message);
    }
}

function handlePOST(request, response) {
    let data = [];

    request.on("data", chunk => {
        try {
            data.push(chunk);
        }
        catch(e) {
            throwBadRequest(response, e.message);
        }
    });

    request.on("end", () => {
        try {
            handleData(JSON.parse(data), function(data) {
                sendOK(response, data);
            }, function(e) {
                sendBadRequest(response, e.message);
            });
        }
        catch(e) {
            sendBadRequest(response, e.message);
        }
    });
}

function handleData(data, successCallback, errorCallback) {
    console.log(data);

    if(!data) {
        errorCallback(errorInvalidData);

        return;
    }

    var mailConfig = {
        service: config.mail.host,
        port: config.mail.port,
        secure: config.mail.secure
    };

    if(config.mail.useCredentials) {
        mailConfig.auth = {
            user: config.mail.username,
            pass: config.mail.password
        }
    }

    nodemailer
        .createTransport(mailConfig).sendMail(data, function(error, info) {
            if (error) {
                errorCallback(error);
            } else {
                successCallback(info.response);
            }
        }
    );
}

function sendOK(response, data) {
    sendJsonResponse(response, 200, data);
}

function sendBadRequest(response, reason) {
    sendJsonResponse(response, 400, {
        reason: reason
    })
}

function sendJsonResponse(response, statusCode, data) {
    response.writeHead(statusCode, defaultHeaders);

    if(data) {
        response.end(JSON.stringify(data));
    }
    else {
        response.end();
    }
}

function safeRequire(module, errorMessage) {
    try {
        return require(module);
    }
    catch (e) {
        console.log(errorMessage);

        return undefined;
    }
}
